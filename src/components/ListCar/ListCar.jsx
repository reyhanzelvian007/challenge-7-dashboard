import style from './ListCar.module.css';
import homeIcon from '../../images/home-icon.svg';
import truckIcon from '../../images/fi_truck.svg';
import { Container } from 'react-bootstrap';
import menuLogo from '../../images/fi_menu.svg';
import accountIcon from '../../images/account-icon.svg';
import chevronDown from '../../images/fi_chevron-down.svg';
import chevronRight from '../../images/chevron-right.svg';
import { useNavigate } from 'react-router-dom';
import Card from '../Card/Card';
import plus from '../../images/fi_plus.svg';


const ListCar = () => {
    const navigate = useNavigate();

    return (
        <>
            <div className={style.mainContainer}>
                <div className={style.leftSide}>
                    <div className={style.sidebar}>
                        <div className={style.sidebarLogo}></div>
                        <div
                            className={style.dashboardLogo}
                            onClick={() => navigate('/')}
                        >
                            <img src={homeIcon} alt="home-icon" />
                            <p>Dashboard</p>
                        </div>
                        <div className={style.carsLogo}>
                            <img src={truckIcon} alt="truck-icon" />
                            <p>Cars</p>
                        </div>
                    </div>
                    <div className={style.leftBar}>
                        <div className={style.leftBarUp}>
                            <div className={style.leftBarLogo}></div>
                        </div>
                        <div className={style.leftBarDown}>
                            <h5>CARS</h5>
                            <div className={style.pStyling}>
                                <p>List Car</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={style.rightSide}>
                    <div>
                        <Container className={style.navbar}>
                            <div className={style.menuLogo}>
                                <img src={menuLogo} alt="menu" />
                            </div>
                            <div className={style.navbarRightSide}>
                                <input type="text" placeholder='Search'/>
                                <button>Search</button>
                                <div className={style.account}>
                                <img src={accountIcon} alt="acc-icon" />
                                <p>Unis Badri</p>
                                <img src={chevronDown} alt="down" />
                                </div>
                            </div>
                        </Container>
                    </div>
                    <div className={style.breadcrumb}>
                        <h6>Cars</h6>
                        <img src={chevronRight} alt="right" />
                        <p>List Car</p>
                    </div>
                    <div className={style.title}>
                        <h4>List Car</h4>
                        <button onClick={() => navigate('/add-new-car')}>
                            <img src={plus} alt="plus" />
                            Add New Car
                        </button>
                    </div>
                    <div className={style.buttonContainer}>
                        <button className={style.buttonAll}>All</button>
                        <button className={style.buttonSmall}>Small</button>
                        <button className={style.buttonMedium}>Medium</button>
                        <button className={style.buttonLarge}>Large</button>
                    </div>
                    <div className={style.cardContainer}>
                        <div className={style.cardContainerRow}>
                            <Card />
                            <Card />
                            <Card />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
};

export default ListCar;