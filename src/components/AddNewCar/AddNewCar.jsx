import style from './AddNewCar.module.css';
import homeIcon from '../../images/home-icon.svg';
import truckIcon from '../../images/fi_truck.svg';
import { Container } from 'react-bootstrap';
import menuLogo from '../../images/fi_menu.svg';
import accountIcon from '../../images/account-icon.svg';
import chevronDown from '../../images/fi_chevron-down.svg';
import chevronRight from '../../images/chevron-right.svg';
import { useNavigate } from 'react-router-dom';


const AddNewCar = () => {
    const navigate = useNavigate();

    const saveButton = () => {
        navigate('/list-car');
        alert('Data Berhasil Disimpan');
    }

    return (
        <>
            <div className={style.mainContainer}>
                <div className={style.leftSide}>
                    <div className={style.sidebar}>
                        <div className={style.sidebarLogo}></div>
                        <div
                            className={style.dashboardLogo}
                            onClick={() => navigate('/')}
                        >
                            <img src={homeIcon} alt="home-icon" />
                            <p>Dashboard</p>
                        </div>
                        <div
                            className={style.carsLogo}
                            onClick={() => navigate('/list-car')}
                        >
                            <img src={truckIcon} alt="truck-icon" />
                            <p>Cars</p>
                        </div>
                    </div>
                    <div className={style.leftBar}>
                        <div className={style.leftBarUp}>
                        <div className={style.leftBarLogo}></div>
                        </div>
                        <div className={style.leftBarDown}>
                        <h5>CARS</h5>
                        <div className={style.pStyling}>
                            <p>List Car</p>
                        </div>
                        </div>
                    </div>
                </div>
                <div className={style.rightSide}>
                    <div>
                        <Container className={style.navbar}>
                        <div className={style.menuLogo}>
                            <img src={menuLogo} alt="menu" />
                        </div>
                        <div className={style.navbarRightSide}>
                            <input type="text" placeholder='Search'/>
                            <button>Search</button>
                            <div className={style.account}>
                                <img src={accountIcon} alt="acc-icon" />
                                <p>Unis Badri</p>
                                <img src={chevronDown} alt="down" />
                            </div>
                        </div>
                        </Container>
                    </div>
                    <div className={style.breadcrumb}>
                        <h6>Cars</h6>
                        <img src={chevronRight} alt="right" />
                        <h6>List Car</h6>
                        <img src={chevronRight} alt="right" />
                        <p>Add New Car</p>
                    </div>
                    <div className={style.title}>
                        <h4>Add New Car</h4>
                    </div>
                    <div className={style.formContainer}>
                        <div className={style.formPoint1}>
                            <p>Nama</p>
                            <input type="text" placeholder='Placeholder' />
                        </div>
                        <div className={style.formPoint}>
                            <p>Harga</p>
                            <input type="text" placeholder='Placeholder' />
                        </div>
                        <div className={style.formPoint}>
                            <p>Foto</p>
                            <div className={style.upload}>
                                <input type="text" placeholder='Placeholder' />
                                <p>File size max. 2MB</p>
                            </div>
                        </div>
                        <div className={style.formPoint}>
                            <p>Start Rent</p>
                            <p>-</p>
                        </div>
                        <div className={style.formPoint}>
                            <p>Finish Rent</p>
                            <p>-</p>
                        </div>
                        <div className={style.formPoint}>
                            <p>Created at</p>
                            <p>-</p>
                        </div>
                        <div className={style.lastFormPoint}>
                            <p>Updated at</p>
                            <p>-</p>
                        </div>
                    </div>
                    <div className={style.buttonContainer}>
                        <button
                            className={style.cancelButton}
                            onClick={() => navigate('/list-car')}
                        >
                            Cancel
                        </button>
                        <button
                            className={style.saveButton}
                            onClick={() => saveButton()}
                        >
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </>
    )
};

export default AddNewCar;