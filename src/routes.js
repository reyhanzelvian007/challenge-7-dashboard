import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AddNewCar from './components/AddNewCar/AddNewCar';
import Dashboard from './components/Dashboard/Dashboard';
import ListCar from './components/ListCar/ListCar';

const RouteApp = () => {
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<Dashboard />} />
                    <Route path='/list-car' element={<ListCar />} />
                    <Route path='/add-new-car' element={<AddNewCar />} />
                </Routes>
            </BrowserRouter>
        </>
    )
};

export default RouteApp;